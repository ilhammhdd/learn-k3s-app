#!/bin/bash

REPO=localhost:5000/learn-k3s-app

podman manifest create $REPO:latest

podman manifest add $REPO:latest docker://$REPO:linux-arm64
podman manifest add $REPO:latest docker://$REPO:linux-amd64

podman manifest push --all $REPO:latest docker://$REPO:latest
